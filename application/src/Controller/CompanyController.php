<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class CompanyController extends AbstractController
{
    #[Route('/companies', name: 'companies')]
    public function index(EntityManagerInterface $em): Response
    {
        $companies = $em->getRepository(Company::class)->findAll();

        return $this->render('company/index.html.twig', ['companies' => $companies]);
    }

    #[Route('/companies/from/{from}/to/{to}', name: 'companies_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $companies = $em->getRepository(Company::class)->findBetween($from, $to);

        return $this->render('company/index.html.twig', [
            'companies' => $companies,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/company/{id}', name: 'company')]
    public function display(Company $company): Response
    {
        return $this->render('company/display.html.twig', ['company' => $company]);
    }
}
