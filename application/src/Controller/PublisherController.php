<?php

namespace App\Controller;

use App\Entity\Publisher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class PublisherController extends AbstractController
{
    #[Route('/publishers', name: 'publishers')]
    public function index(EntityManagerInterface $em): Response
    {
        $publishers = $em->getRepository(Publisher::class)->findAll();

        return $this->render('publisher/index.html.twig', ['publishers' => $publishers]);
    }

    #[Route('/publisher/{id}', name: 'publisher')]
    public function display(Publisher $publisher): Response
    {
        return $this->render('publisher/display.html.twig', ['publisher' => $publisher]);
    }
}
