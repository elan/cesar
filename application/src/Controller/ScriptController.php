<?php

namespace App\Controller;

use App\Entity\Form;
use App\Entity\Qualifier;
use App\Entity\Script;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class ScriptController extends AbstractController
{
    #[Route('/scripts', name: 'scripts')]
    public function index(EntityManagerInterface $em): Response
    {
        $forms = $em->getRepository(Form::class)->findAll();
        $qualifiers = $em->getRepository(Qualifier::class)->findWithScript();
        $genres = $em->getRepository(Script::class)->findDistinctGenres();

        return $this->render('script/index.html.twig', [
            'genres' => $genres,
            'forms' => $forms,
            'qualifiers' => $qualifiers

        ]);
    }

    #[Route('/scripts/form/{id}', name: 'scripts_form')]
    public function form(Form $form, EntityManagerInterface $em): Response
    {
        $scripts = $em->getRepository(Script::class)->findWithForm($form);

        return $this->render('script/list.html.twig', [
            'scripts' => $scripts,
            'criteria' => $form->getName()
        ]);
    }

    #[Route('/scripts/qualifier/{id}', name: 'scripts_qualifier')]
    public function qualifier(Qualifier $qualifier, EntityManagerInterface $em): Response
    {
        $scripts = $em->getRepository(Script::class)->findWithQualifier($qualifier);

        return $this->render('script/list.html.twig', [
            'scripts' => $scripts,
            'criteria' => $qualifier->getName()
        ]);
    }

    #[Route('/scripts/genre/{genre}', name: 'scripts_genre')]
    public function genre($genre, EntityManagerInterface $em): Response
    {
        $scripts = $em->getRepository(Script::class)->findByGenre($genre);

        return $this->render('script/list.html.twig', [
            'scripts' => $scripts,
            'criteria' => $genre
        ]);
    }

    #[Route('/script/{id}', name: 'script')]
    public function display(Script $script): Response
    {
        return $this->render('script/display.html.twig', ['script' => $script]);
    }
}
