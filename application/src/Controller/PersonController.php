<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Skill;
use App\Service\SkillManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class PersonController extends AbstractController
{
    #[Route('/persons', name: 'persons')]
    public function index(EntityManagerInterface $em): Response
    {
        $persons = $em->getRepository(Person::class)->findAll();

        return $this->render('person/index.html.twig', ['persons' => $persons]);
    }

    #[Route('/person/{id}', name: 'person')]
    public function display(Person $person): Response
    {
        return $this->render('person/display.html.twig', ['person' => $person]);
    }
    
    #[Route('/persons/from/{from}/to/{to}', name: 'persons_between')]
    public function between(EntityManagerInterface $em, $from, $to): Response
    {
        $persons = $em->getRepository(Person::class)->findBetween($from, $to);

        return $this->render('person/index.html.twig', [
            'persons' => $persons,
            'from' => new \DateTime($from),
            'to' => new \DateTime($to)
        ]);
    }

    #[Route('/persons/skills', name: 'person_skills')]
    public function skills(SkillManager $sm): Response
    {   
        $skills = $sm->findWithPerson();

        return $this->render('person/skills.html.twig', ['skills' => $skills]);
    }

    #[Route('/persons/skill/{id}', name: 'persons_skill')]
    public function skill(Skill $skill, EntityManagerInterface $em): Response
    {   
        $persons = $em->getRepository(Person::class)->findWithSkill($skill);

        return $this->render('person/index.html.twig', [
            'skill' => $skill,
            'persons' => $persons
        ]);
    }
}
