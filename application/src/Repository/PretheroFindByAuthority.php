<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Person;
use App\Entity\Script;
use App\Entity\Performance;
use App\Entity\Publication;
use App\Entity\Publisher;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @extends ServiceEntityRepository<Person>
 *
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PretheroFindByAuthority 
{
   
    protected $registry;
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
       $this->registry = $registry;
       $this->em = $em;
    }

    public function findPerson(string $personUri): Person 
    {

        $sql = '
        SELECT p.id, p.firstname, p.lastname FROM person p
        WHERE JSON_SEARCH(p.authorities, "one", :authorityId) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Person", "p");
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'firstname', 'firstname');
        $rsm->addFieldResult('p', 'lastname', 'lastname');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $personUri);
        $person = $query->getSingleResult();
        
        return $person;
    }

    public function findLocation(string $locationUri): Location 
    {

        $wildcardedUri = "%" . $locationUri . "%";
        $sql = '
        SELECT l.id, l.name, l.parent_id FROM location l
        WHERE JSON_SEARCH(l.authorities, "one", :authorityId ) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Location", "l");
        $rsm->addFieldResult('l', 'id', 'id');
        $rsm->addFieldResult('l', 'name', 'name');
        $rsm->addFieldResult('l', 'parent', 'parent_id');

        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $wildcardedUri);
        $location = $query->getSingleResult();

        return $location;
    }

    public function findScript(string $scriptUri): Script 
    {
        $sql = '
        SELECT s.id FROM script s
        WHERE JSON_SEARCH(s.authorities, "one", :authorityId) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Script", "s");
        $rsm->addFieldResult('s', 'id', 'id');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $scriptUri);
        $script = $query->getSingleResult();

        return $script;
    }
    public function findPerformance(string $performanceUri): Performance 
    {
        $sql = '
        SELECT perf.id FROM performance perf
        WHERE JSON_SEARCH(perf.authorities, "one", :authorityId) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Performance", "perf");
        $rsm->addFieldResult('perf', 'id', 'id');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $performanceUri);
        $performance = $query->getSingleResult();

        return $performance;
    }

    public function findPublication(string $publicationUri): Publication 
    {
        $sql = '
        SELECT publi.id FROM publication publi
        WHERE JSON_SEARCH(publi.authorities, "one", :authorityId) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Publication", "publi");
        $rsm->addFieldResult('publi', 'id', 'id');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $publicationUri);
        $publication = $query->getSingleResult();

        return $publication;
    }

    public function findPublisher(string $publisherUri): Publisher 
    {
        $sql = '
        SELECT publisher.id FROM publisher
        WHERE JSON_SEARCH(publisher.authorities, "one", :authorityId) IS NOT NULL
        ';
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata("App\Entity\Publisher", "publisher");
        $rsm->addFieldResult('publisher', 'id', 'id');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':authorityId', $publisherUri);
        $publisher = $query->getSingleResult();

        return $publisher;
    }
    
}




