<?php

namespace App\Repository;

use App\Entity\Script;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Script>
 *
 * @method Script|null find($id, $lockMode = null, $lockVersion = null)
 * @method Script|null findOneBy(array $criteria, array $orderBy = null)
 * @method Script[]    findAll()
 * @method Script[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScriptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Script::class);
    }


    
    public function save(Script $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Script $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   public function findWithForm($form): array
   {
       return $this->createQueryBuilder('s')
           ->andWhere(':form MEMBER OF s.forms')
           ->setParameter('form', $form->getId())
           ->getQuery()
           ->getResult()
       ;
   }

   public function findWithQualifier($qualifier): array
   {
       return $this->createQueryBuilder('s')
           ->orWhere(':qualifier MEMBER OF s.qualifiers')
           ->orWhere(':qualifierParent MEMBER OF s.qualifiers')
           ->setParameter('qualifier', $qualifier->getId())
           ->setParameter('qualifierParent', $qualifier->getParent() ? $qualifier->getParent()->getId() : null)
           ->getQuery()
           ->getResult()
       ;
   }

   public function findDistinctGenres(): array
   {
       return $this->createQueryBuilder('s')
                ->select('s.genre')
                ->orderBy('s.genre', 'ASC')
                ->distinct()
                ->getQuery()
                ->getResult()
       ;
   }


//    /**
//     * @return Script[] Returns an array of Script objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Script
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
