<?php

namespace App\Repository;

use App\Entity\Qualifier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Qualifier>
 *
 * @method Qualifier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Qualifier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Qualifier[]    findAll()
 * @method Qualifier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualifierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Qualifier::class);
    }

    public function save(Qualifier $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Qualifier $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    
   public function findWithScript(): array
   {
       return $this->createQueryBuilder('q')
        ->innerJoin('q.scripts', 's', 'WITH', 'q MEMBER OF s.qualifiers')
        ->getQuery()
        ->getResult()
       ;
   }

//    /**
//     * @return Qualifier[] Returns an array of Qualifier objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('q')
//            ->andWhere('q.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('q.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Qualifier
//    {
//        return $this->createQueryBuilder('q')
//            ->andWhere('q.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
