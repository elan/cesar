<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:user-role',
    description: 'Add a short description for your command',
)]
class UserRoleCommand extends Command
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('mail', InputArgument::REQUIRED, 'Argument description')
            ->addArgument('role', InputArgument::REQUIRED, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $mail = $input->getArgument('mail');
        $role = $input->getArgument('role');

        if ($mail && $role) {
            $roles = ["ROLE_USER", "ROLE_ADMIN", "ROLE_SUPER_ADMIN"];
            if (!in_array($role, $roles)) {
                $io->warning("Le rôle renseigné doit correspondre à un de ceux-ci : " . implode(" - ", $roles));
                return Command::FAILURE;
            }

            if (!$user = $this->em->getRepository(User::class)->findOneByEmail($mail)) {
                $io->warning("Pas d'utilisateur avec ce mail");
                return Command::FAILURE;
            }

            $role = [$role];
            $user->setRoles($role);

            $this->em->persist($user);
            $this->em->flush();

            $io->success('Changement de rôle effectué.');
        }

        return Command::SUCCESS;
    }
}
