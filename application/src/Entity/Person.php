<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\OriginalIdTrait;
use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: PersonRepository::class)]
#[Index(columns: ["original_id"])]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class Person
{
    use OriginalIdTrait;
    use AuthoritiesTrait;
    use SourcesTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $birthMin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $birthMax = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $deathMin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $deathMax = null;

    #[ORM\Column]
    private ?int $gender = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $firstname = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lastname = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $particle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pseudonym = null;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: Note::class)]
    private Collection $notes;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: CompanyMembership::class, orphanRemoval: true)]
    private Collection $companyMemberships;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: PerformancePerson::class, orphanRemoval: true)]
    private Collection $performances;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: ScriptCreator::class, orphanRemoval: true)]
    private Collection $creations;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: PublisherMembership::class, orphanRemoval: true)]
    private Collection $publisherMemberships;

    #[ORM\ManyToOne]
    private ?Location $nationality = null;

    #[ORM\ManyToOne]
    private ?SocialTitle $socialTitle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $wikidata = null;

    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'persons')]
    private Collection $skills;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->companyMemberships = new ArrayCollection();
        $this->performances = new ArrayCollection();
        $this->creations = new ArrayCollection();
        $this->publisherMemberships = new ArrayCollection();
        $this->skills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBirthMin(): ?\DateTimeInterface
    {
        return $this->birthMin;
    }

    public function setBirthMin(?\DateTimeInterface $birthMin): self
    {
        $this->birthMin = $birthMin;

        return $this;
    }

    public function getBirthMax(): ?\DateTimeInterface
    {
        return $this->birthMax;
    }

    public function setBirthMax(?\DateTimeInterface $birthMax): self
    {
        $this->birthMax = $birthMax;

        return $this;
    }

    public function getDeathMin(): ?\DateTimeInterface
    {
        return $this->deathMin;
    }

    public function setDeathMin(?\DateTimeInterface $deathMin): self
    {
        $this->deathMin = $deathMin;

        return $this;
    }

    public function getDeathMax(): ?\DateTimeInterface
    {
        return $this->deathMax;
    }

    public function setDeathMax(?\DateTimeInterface $deathMax): self
    {
        $this->deathMax = $deathMax;

        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getParticle(): ?string
    {
        return $this->particle;
    }

    public function setParticle(?string $particle): self
    {
        $this->particle = $particle;

        return $this;
    }

    public function getPseudonym(): ?string
    {
        return $this->pseudonym;
    }

    public function setPseudonym(?string $pseudonym): self
    {
        $this->pseudonym = $pseudonym;

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setPerson($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getPerson() === $this) {
                $note->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompanyMembership>
     */
    public function getCompanyMemberships(): Collection
    {
        return $this->companyMemberships;
    }

    public function addCompanyMembership(CompanyMembership $companyMembership): self
    {
        if (!$this->companyMemberships->contains($companyMembership)) {
            $this->companyMemberships->add($companyMembership);
            $companyMembership->setPerson($this);
        }

        return $this;
    }

    public function removeCompanyMembership(CompanyMembership $companyMembership): self
    {
        if ($this->companyMemberships->removeElement($companyMembership)) {
            // set the owning side to null (unless already changed)
            if ($companyMembership->getPerson() === $this) {
                $companyMembership->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PerformancePerson>
     */
    public function getPerformances(): Collection
    {
        return $this->performances;
    }

    public function addPerformance(PerformancePerson $performance): self
    {
        if (!$this->performances->contains($performance)) {
            $this->performances->add($performance);
            $performance->setPerson($this);
        }

        return $this;
    }

    public function removePerformance(PerformancePerson $performance): self
    {
        if ($this->performances->removeElement($performance)) {
            // set the owning side to null (unless already changed)
            if ($performance->getPerson() === $this) {
                $performance->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ScriptCreator>
     */
    public function getCreations(): Collection
    {
        return $this->creations;
    }

    public function addCreation(ScriptCreator $creation): self
    {
        if (!$this->creations->contains($creation)) {
            $this->creations->add($creation);
            $creation->setPerson($this);
        }

        return $this;
    }

    public function removeCreation(ScriptCreator $creation): self
    {
        if ($this->creations->removeElement($creation)) {
            // set the owning side to null (unless already changed)
            if ($creation->getPerson() === $this) {
                $creation->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PublisherMembership>
     */
    public function getPublisherMemberships(): Collection
    {
        return $this->publisherMemberships;
    }

    public function addPublisherMembership(PublisherMembership $publisherMembership): self
    {
        if (!$this->publisherMemberships->contains($publisherMembership)) {
            $this->publisherMemberships->add($publisherMembership);
            $publisherMembership->setPerson($this);
        }

        return $this;
    }

    public function removePublisherMembership(PublisherMembership $publisherMembership): self
    {
        if ($this->publisherMemberships->removeElement($publisherMembership)) {
            // set the owning side to null (unless already changed)
            if ($publisherMembership->getPerson() === $this) {
                $publisherMembership->setPerson(null);
            }
        }

        return $this;
    }

    public function getNationality(): ?Location
    {
        return $this->nationality;
    }

    public function setNationality(?Location $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getSocialTitle(): ?SocialTitle
    {
        return $this->socialTitle;
    }

    public function setSocialTitle(?SocialTitle $socialTitle): self
    {
        $this->socialTitle = $socialTitle;

        return $this;
    }

    public function getWikidata(): ?string
    {
        return $this->wikidata;
    }

    public function setWikidata(?string $wikidata): self
    {
        $this->wikidata = $wikidata;

        return $this;
    }

    /**
     * @return Collection<int, Skill>
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills->add($skill);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        $this->skills->removeElement($skill);

        return $this;
    }

}
