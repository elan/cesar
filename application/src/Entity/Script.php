<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\OriginalIdTrait;
use App\Entity\Traits\TitleTrait;
use App\Repository\ScriptRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: ScriptRepository::class)]
#[Index(columns: ["original_id"])]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class Script
{
    use OriginalIdTrait;
    use TitleTrait;
    use AuthoritiesTrait;
    use SourcesTrait;    
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $actNumber = null;

    #[ORM\OneToMany(mappedBy: 'script', targetEntity: ScriptCreator::class, orphanRemoval: true)]
    private Collection $creators;

    #[ORM\OneToMany(mappedBy: 'script', targetEntity: Performance::class, orphanRemoval: true)]
    private Collection $performances;

    #[ORM\ManyToMany(targetEntity: Language::class)]
    private Collection $languages;

    #[ORM\OneToMany(mappedBy: 'script', targetEntity: Publication::class, orphanRemoval: true)]
    private Collection $publications;

    #[ORM\ManyToMany(targetEntity: Form::class, inversedBy: 'scripts')]
    private Collection $forms;

    #[ORM\ManyToMany(targetEntity: Qualifier::class, inversedBy: 'scripts')]
    private Collection $qualifiers;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $genre = null;

    #[ORM\OneToMany(mappedBy: 'script', targetEntity: Manuscript::class, orphanRemoval: true)]
    private Collection $manuscripts;

    public function __construct()
    {
        $this->creators = new ArrayCollection();
        $this->performances = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->forms = new ArrayCollection();
        $this->qualifiers = new ArrayCollection();
        $this->manuscripts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActNumber(): ?int
    {
        return $this->actNumber;
    }

    public function setActNumber(?int $actNumber): self
    {
        $this->actNumber = $actNumber;

        return $this;
    }

    /**
     * @return Collection<int, ScriptCreator>
     */
    public function getCreators(): Collection
    {
        return $this->creators;
    }

    public function addCreator(ScriptCreator $creator): self
    {
        if (!$this->creators->contains($creator)) {
            $this->creators->add($creator);
            $creator->setScript($this);
        }

        return $this;
    }

    public function removeCreator(ScriptCreator $creator): self
    {
        if ($this->creators->removeElement($creator)) {
            // set the owning side to null (unless already changed)
            if ($creator->getScript() === $this) {
                $creator->setScript(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Performance>
     */
    public function getPerformances(): Collection
    {
        return $this->performances;
    }

    public function addPerformance(Performance $performance): self
    {
        if (!$this->performances->contains($performance)) {
            $this->performances->add($performance);
            $performance->setScript($this);
        }

        return $this;
    }

    public function removePerformance(Performance $performance): self
    {
        if ($this->performances->removeElement($performance)) {
            // set the owning side to null (unless already changed)
            if ($performance->getScript() === $this) {
                $performance->setScript(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Language>
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(Language $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages->add($language);
        }

        return $this;
    }

    public function removeLanguage(Language $language): self
    {
        $this->languages->removeElement($language);

        return $this;
    }

    /**
     * @return Collection<int, Publication>
     */
    public function getPublications(): Collection
    {
        return $this->publications;
    }

    public function addPublication(Publication $publication): self
    {
        if (!$this->publications->contains($publication)) {
            $this->publications->add($publication);
            $publication->setScript($this);
        }

        return $this;
    }

    public function removePublication(Publication $publication): self
    {
        if ($this->publications->removeElement($publication)) {
            // set the owning side to null (unless already changed)
            if ($publication->getScript() === $this) {
                $publication->setScript(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Form>
     */
    public function getForms(): Collection
    {
        return $this->forms;
    }

    public function addForm(Form $form): self
    {
        if (!$this->forms->contains($form)) {
            $this->forms->add($form);
        }

        return $this;
    }

    public function removeForm(Form $form): self
    {
        $this->forms->removeElement($form);

        return $this;
    }

    /**
     * @return Collection<int, Qualifier>
     */
    public function getQualifiers(): Collection
    {
        return $this->qualifiers;
    }

    public function addQualifier(Qualifier $qualifier): self
    {
        if (!$this->qualifiers->contains($qualifier)) {
            $this->qualifiers->add($qualifier);
        }

        return $this;
    }

    public function removeQualifier(Qualifier $qualifier): self
    {
        $this->qualifiers->removeElement($qualifier);

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(?string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection<int, Manuscript>
     */
    public function getManuscripts(): Collection
    {
        return $this->manuscripts;
    }

    public function addManuscript(Manuscript $manuscript): self
    {
        if (!$this->manuscripts->contains($manuscript)) {
            $this->manuscripts->add($manuscript);
            $manuscript->setScript($this);
        }

        return $this;
    }

    public function removeManuscript(Manuscript $manuscript): self
    {
        if ($this->manuscripts->removeElement($manuscript)) {
            // set the owning side to null (unless already changed)
            if ($manuscript->getScript() === $this) {
                $manuscript->setScript(null);
            }
        }

        return $this;
    }
}
