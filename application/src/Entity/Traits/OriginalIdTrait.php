<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait OriginalIdTrait
{
    #[ORM\Column(nullable: true)]
    private ?string $originalId = null;


    public function getOriginalId(): ?string
    {
        return $this->originalId;
    }

    public function setOriginalId(?string $originalId): self
    {
        $this->originalId = $originalId;

        return $this;
    }
}
