<?php

namespace App\Entity\Traits;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait TitleTrait
{
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $subtitle = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $alternativeTitle = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $alternativeSubtitle = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getAlternativeTitle(): ?string
    {
        return $this->alternativeTitle;
    }

    public function setAlternativeTitle(?string $alternativeTitle): self
    {
        $this->alternativeTitle = $alternativeTitle;

        return $this;
    }

    public function getAlternativeSubtitle(): ?string
    {
        return $this->alternativeSubtitle;
    }

    public function setAlternativeSubtitle(?string $alternativeSubtitle): self
    {
        $this->alternativeSubtitle = $alternativeSubtitle;

        return $this;
    }
}
