<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait SourcesTrait
{
    #[ORM\Column(nullable: true)]
    private array $sources = [];


    public function getSources(): array
    {
        return $this->sources;
    }

    public function setSources(?array $sources): self
    {
        $this->sources = $sources;

        return $this;
    }
}
