<?php

namespace App\Entity\Traits;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait IntervalDoubleTrait
{

  #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startMin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startMax = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endMin = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endMax = null;


    public function getStartMin(): ?\DateTimeInterface
    {
        return $this->startMin;
    }

    public function setStartMin(?\DateTimeInterface $startMin): self
    {
        $this->startMin = $startMin;

        return $this;
    }

    public function getStartMax(): ?\DateTimeInterface
    {
        return $this->startMax;
    }

    public function setStartMax(?\DateTimeInterface $startMax): self
    {
        $this->startMax = $startMax;

        return $this;
    }

    public function getEndMin(): ?\DateTimeInterface
    {
        return $this->endMin;
    }

    public function setEndMin(?\DateTimeInterface $endMin): self
    {
        $this->endMin = $endMin;

        return $this;
    }

    public function getEndMax(): ?\DateTimeInterface
    {
        return $this->endMax;
    }

    public function setEndMax(?\DateTimeInterface $endMax): self
    {
        $this->endMax = $endMax;

        return $this;
    }
}
