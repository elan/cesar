<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\IntervalSimpleTrait;
use App\Entity\Traits\OriginalIdTrait;
use App\Entity\Traits\TitleTrait;
use App\Repository\PublicationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: PublicationRepository::class)]
#[Index(columns: ["original_id"])]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class Publication
{
    use IntervalSimpleTrait;
    use OriginalIdTrait;
    use TitleTrait;
    use AuthoritiesTrait;
    use SourcesTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'publications')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Script $script = null;

    #[ORM\Column(nullable: true)]
    private ?int $format = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $dateComment = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $approvalStart = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $approvalEnd = null;

    #[ORM\ManyToOne(inversedBy: 'publications')]
    private ?Anthology $anthology = null;

    #[ORM\Column(nullable: true)]
    private ?int $anthologyVolume = null;

    #[ORM\ManyToOne(inversedBy: 'publications')]
    private ?Publisher $publisher = null;

    #[ORM\ManyToMany(targetEntity: Location::class)]
    private Collection $location;

    #[ORM\OneToMany(mappedBy: 'publication', targetEntity: Copy::class, orphanRemoval: true)]
    private Collection $copies;

    public function __construct()
    {
        $this->location = new ArrayCollection();
        $this->copies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScript(): ?Script
    {
        return $this->script;
    }

    public function setScript(?Script $script): self
    {
        $this->script = $script;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(?int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getDateComment(): ?string
    {
        return $this->dateComment;
    }

    public function setDateComment(?string $dateComment): self
    {
        $this->dateComment = $dateComment;

        return $this;
    }

    public function getApprovalStart(): ?\DateTimeInterface
    {
        return $this->approvalStart;
    }

    public function setApprovalStart(?\DateTimeInterface $approvalStart): self
    {
        $this->approvalStart = $approvalStart;

        return $this;
    }

    public function getApprovalEnd(): ?\DateTimeInterface
    {
        return $this->approvalEnd;
    }

    public function setApprovalEnd(?\DateTimeInterface $approvalEnd): self
    {
        $this->approvalEnd = $approvalEnd;

        return $this;
    }

    public function getAnthology(): ?Anthology
    {
        return $this->anthology;
    }

    public function setAnthology(?Anthology $anthology): self
    {
        $this->anthology = $anthology;

        return $this;
    }

    public function getAnthologyVolume(): ?int
    {
        return $this->anthologyVolume;
    }

    public function setAnthologyVolume(?int $anthologyVolume): self
    {
        $this->anthologyVolume = $anthologyVolume;

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @return Collection<int, Location>
     */
    public function getLocation(): Collection
    {
        return $this->location;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->location->contains($location)) {
            $this->location->add($location);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        $this->location->removeElement($location);

        return $this;
    }

    /**
     * @return Collection<int, Copy>
     */
    public function getCopies(): Collection
    {
        return $this->copies;
    }

    public function addCopy(Copy $copy): self
    {
        if (!$this->copies->contains($copy)) {
            $this->copies->add($copy);
            $copy->setPublication($this);
        }

        return $this;
    }

    public function removeCopy(Copy $copy): self
    {
        if ($this->copies->removeElement($copy)) {
            // set the owning side to null (unless already changed)
            if ($copy->getPublication() === $this) {
                $copy->setPublication(null);
            }
        }

        return $this;
    }
}
