<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\IntervalSimpleTrait;
use App\Entity\Traits\OriginalIdTrait;
use App\Entity\Traits\TitleTrait;
use App\Repository\PerformanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;

#[ORM\Entity(repositoryClass: PerformanceRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class Performance
{
    use IntervalSimpleTrait;
    use OriginalIdTrait;
    use TitleTrait;
    use AuthoritiesTrait;
    use SourcesTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'performances')]
    private ?Company $company = null;

    #[ORM\Column(nullable: true)]
    private ?int $actNumber = null;

    #[ORM\OneToMany(mappedBy: 'performance', targetEntity: PerformancePerson::class, orphanRemoval: true)]
    private Collection $personParticipations;

    #[ORM\ManyToOne(inversedBy: 'performances')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Script $script = null;

    #[ORM\ManyToOne(inversedBy: 'performances')]
    private ?Location $location = null;

    public function __construct()
    {
        $this->personParticipations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getActNumber(): ?int
    {
        return $this->actNumber;
    }

    public function setActNumber(?int $actNumber): self
    {
        $this->actNumber = $actNumber;

        return $this;
    }

    /**
     * @return Collection<int, PerformancePerson>
     */
    public function getPersonParticipations(): Collection
    {
        return $this->personParticipations;
    }

    public function addPersonParticipation(PerformancePerson $personParticipation): self
    {
        if (!$this->personParticipations->contains($personParticipation)) {
            $this->personParticipations->add($personParticipation);
            $personParticipation->setPerformance($this);
        }

        return $this;
    }

    public function removePersonParticipation(PerformancePerson $personParticipation): self
    {
        if ($this->personParticipations->removeElement($personParticipation)) {
            // set the owning side to null (unless already changed)
            if ($personParticipation->getPerformance() === $this) {
                $personParticipation->setPerformance(null);
            }
        }

        return $this;
    }

    public function getScript(): ?Script
    {
        return $this->script;
    }

    public function setScript(?Script $script): self
    {
        $this->script = $script;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }
}
