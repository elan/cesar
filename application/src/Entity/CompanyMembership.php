<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Traits\IntervalDoubleTrait;
use App\Repository\CompanyMembershipRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: CompanyMembershipRepository::class)]
#[ApiResource(
    // normalizationContext: ['groups' => ['test']],
    operations: [
        new Get(),
        new GetCollection()
    ],
    graphQlOperations: [
        new Query(),
    ]
)]
#[ApiFilter(SearchFilter::class, strategy: 'partial')]
#[ApiFilter(ExistsFilter::class)]
#[ApiFilter(DateFilter::class)]

class CompanyMembership
{   
    use IntervalDoubleTrait;
    use SourcesTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /**
     * Summary of id
     * @var int|null
     */
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'companyMemberships')]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * Summary of person
     * @var Person|null
     */
    private ?Person $person = null;

    #[Groups('test')]
    #[ORM\ManyToOne(inversedBy: 'memberships')]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * Summary of company
     * @var Company|null
     */
    private ?Company $company = null;

    #[Groups('test')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * Summary of activity
     * @var Activity|null
     */
    private ?Activity $activity = null;

    /**
     * Summary of getId
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Summary of getPerson
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * Summary of setPerson
     * @param Person|null $person
     * @return CompanyMembership
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Summary of getCompany
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * Summary of setCompany
     * @param Company|null $company
     * @return CompanyMembership
     */
    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Summary of getActivity
     * @return Activity|null
     */
    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    /**
     * Summary of setActivity
     * @param Activity|null $activity
     * @return CompanyMembership
     */
    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }
}
