<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ManuscriptRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\OriginalIdTrait;
use App\Entity\Traits\IntervalSimpleTrait;
use App\Entity\Traits\TitleTrait;
use App\Entity\Traits\AuthoritiesTrait;
use App\Entity\Traits\SourcesTrait;


#[ORM\Entity(repositoryClass: ManuscriptRepository::class)]
#[ApiResource]
class Manuscript
{
    use IntervalSimpleTrait;
    use OriginalIdTrait;
    use TitleTrait;
    use AuthoritiesTrait;
    use SourcesTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'manuscripts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Script $script = null;

    #[ORM\ManyToOne(inversedBy: 'manuscripts')]
    private ?Library $library = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $callNumber = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScript(): ?Script
    {
        return $this->script;
    }

    public function setScript(?Script $script): self
    {
        $this->script = $script;

        return $this;
    }

    public function getLibrary(): ?Library
    {
        return $this->library;
    }

    public function setLibrary(?Library $library): self
    {
        $this->library = $library;

        return $this;
    }

    public function getCallNumber(): ?string
    {
        return $this->callNumber;
    }

    public function setCallNumber(?string $callNumber): self
    {
        $this->callNumber = $callNumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
