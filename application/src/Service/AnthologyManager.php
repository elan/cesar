<?php

namespace App\Service;

use App\Entity\Anthology;
use App\Entity\Location;
use Doctrine\ORM\EntityManagerInterface;

class AnthologyManager
{
    private $em;
    private $logManager;

    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;

    }


    public function create($originalId, $format, $start, $end, $title, $subtitle, $authorities, $sources)
    {
        $anthology = new Anthology();
        $anthology->setOriginalId($originalId);
        $anthology->setFormat($format);
        $anthology->setStart($start);
        $anthology->setEnd($end);
        $anthology->setTitle($title);
        $anthology->setSubtitle($subtitle);

        $anthology->setAuthorities($authorities);
        $anthology->setSources($sources);

        $this->em->persist($anthology);

        return $anthology;
    }

    public function import($lines)
    {
        echo "Import anthology \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $format = intval($this->testEmpty($line[1]));
            $start = $this->testDate($line[2]);
            $end = $this->testDate($line[3]);

            $title = $this->testEmpty($line[9]);
            $subtitle = $this->testEmpty($line[10]);

            $authorities = [];
            $sources = ["origin" => "cesar"];

            $this->create($originalId, $format, $start, $end, $title, $subtitle, $authorities, $sources);
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importAnthologyPublicationLocations($lines)
    {
        echo "Import Anthology Publication Location \n";

        $this->logManager->write("AnthologyId | locationId \n");

        foreach ($lines as $line) {
            $anthologyId = preg_replace('/[^0-9]/', '', $line[0]);
            $anthologyId = intval($anthologyId);

            $locationId = preg_replace('/[^0-9]/', '', $line[1]);
            $locationId = intval($locationId);
            
            $anthology = $this->em->getRepository(Anthology::class)->findOneByOriginalId($anthologyId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($locationId);

            if (!$anthology || !$location ) {
                $this->logManager->write($anthologyId . " " . $locationId . "\n");

            } else {
                $anthology->addLocation($location);
                $this->em->persist($anthology);
            }

        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
