<?php

namespace App\Service;

use App\Service\ActivityManager;
use App\Service\AnthologyManager;
use App\Service\CommunityTypeManager;
use App\Service\CompanyManager;
use App\Service\LibraryManager;
use App\Service\LocationManager;
use App\Service\ManuscriptManager;
use App\Service\NoteManager;
use App\Service\PerformanceManager;
use App\Service\PersonManager;
use App\Service\PublicationManager;
use App\Service\ScriptManager;
use App\Service\UserManager;

use App\Service\prethero\LocationManager as PretheroLocManager;
use App\Service\prethero\PersonManager as PretheroPersonManager;
use App\Service\prethero\ScriptManager as PretheroScriptManager;
use App\Service\prethero\PublicationManager as PretheroPublicationManager;
use App\Service\prethero\PerformanceManager as PretheroPerformanceManager;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Stopwatch\Stopwatch;

class ImportManager
{
    private $pm;
    private $am;
    private $nm;
    private $um;
    private $ctm;
    private $cpm;
    private $pfm;
    private $scm;
    private $skm;
    private $logManager;
    private $languageManager;
    private $publicationManager;
    private $anthologyManager;
    private $locationManager;

    private $pretheroLocManager;
    private $pretheroPersonManager;
    private $pretheroScriptManager;
    private $pretheroPublicationManager;
    private $pretheroPerformanceManager;

    private $libraryManager;
    private $manuscriptManager;
    private $copyManager;

        public function __construct(
        PersonManager $pm,
        ActivityManager $am,
        NoteManager $nm,
        UserManager $um,
        CommunityTypeManager $ctm,
        CompanyManager $cpm,
        PerformanceManager $pfm,
        ScriptManager $scm,
        SkillManager $skm,
        LogManager $logManager,
        LanguageManager $languageManager,
        PublicationManager $publicationManager,
        AnthologyManager $anthologyManager,
        LocationManager $locationManager,
        LibraryManager $libraryManager,
        ManuscriptManager $manuscriptManager,
        CopyManager $copyManager,

        PretheroLocManager $pretheroLocManager,
        PretheroPersonManager $pretheroPersonManager,
        PretheroScriptManager $pretheroScriptManager,
        PretheroPublicationManager $pretheroPublicationManager,
        PretheroPerformanceManager $pretheroPerformanceManager,



    ) {
        $this->pm = $pm;
        $this->am = $am;
        $this->nm = $nm;
        $this->um = $um;
        $this->ctm = $ctm;
        $this->cpm = $cpm;
        $this->pfm = $pfm;
        $this->scm = $scm;
        $this->skm = $skm;
        $this->logManager = $logManager;
        $this->languageManager = $languageManager;
        $this->publicationManager = $publicationManager;
        $this->anthologyManager = $anthologyManager;
        $this->locationManager = $locationManager;
        $this->libraryManager = $libraryManager;
        $this->manuscriptManager = $manuscriptManager;
        $this->copyManager = $copyManager;

        $this->pretheroLocManager = $pretheroLocManager;
        $this->pretheroPersonManager = $pretheroPersonManager;
        $this->pretheroScriptManager = $pretheroScriptManager;
        $this->pretheroPublicationManager = $pretheroPublicationManager;
        $this->pretheroPerformanceManager = $pretheroPerformanceManager;





    }

    public function init()
    {
        $this->logManager->delete();
        $stopwatch = new Stopwatch();
        $stopwatch->start('import');




        $stopwatch->start('location');
        $locationTypes = $this->getCsvLines("location_type.csv");
        $this->locationManager->importType($locationTypes);

        $locations = $this->getCsvLines("location.csv");
        $this->locationManager->import($locations);

        $locationsLocationTypes = $this->getCsvLines("location_location-type.csv");
        $this->locationManager->importLocationsLocationType($locationsLocationTypes);
        $event = $stopwatch->stop('location');
        echo $event ."\n";
        gc_collect_cycles();

        //PRETHERO LOCATIONS
        $pretheroLocations = $this->getJsonFile("places.json");
        $this->pretheroLocManager->pretheroLocations($pretheroLocations);
        $this->pretheroLocManager->pretheroLocationsLocationType($pretheroLocations);


        $stopwatch->start('activity');
        $activities = $this->getCsvLines("activity.csv");
        $this->am->import($activities);
        $this->am->importParent($activities);
        $event = $stopwatch->stop('activity');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('skill');
        $activities = $this->getCsvLines("activity.csv");
        $skills = $this->getCsvLines("skill.csv");
        $this->skm->import($skills);
        $this->skm->importParent($skills);
        $event = $stopwatch->stop('skill');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('lang');
        $languages = $this->getCsvLines("lang.csv");
        $this->languageManager->import($languages);
        $event = $stopwatch->stop('lang');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('person');
        $socialTitle = $this->getCsvLines("social_title.csv");
        $this->pm->importSocialTitle($socialTitle);

        $persons = $this->getCsvLines("person.csv");
        $this->pm->import($persons);
        $event = $stopwatch->stop('person');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('person_skill');
        $personSkill = $this->getCsvLines("person_skill.csv");
        $this->pm->importSkill($personSkill);
        $event = $stopwatch->stop('person_skill');
        echo $event ."\n";
        gc_collect_cycles();

        // PRETHERO PERSONS - SKILLS
        $pretheroPersons = $this->getJsonFile("persons.json");
        $this->pretheroPersonManager->importPretheroPersons($pretheroPersons);
        $this->pretheroPersonManager->importPretheroPersonSkills($pretheroPersons);

        $stopwatch->start('contributor');
        $users = $this->getCsvLines("contributor.csv");
        $this->um->import($users);
        $event = $stopwatch->stop('contributor');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('note');
        $notes = $this->getCsvLines("tsar_note.csv");
        $this->nm->import($notes);
        $event = $stopwatch->stop('note');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('note_author');
        $notesUser = $this->getCsvLines("tsar_note_author.csv");
        $this->nm->importAuthor($notesUser);
        $event = $stopwatch->stop('note_author');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('communityType');
        $communitytypes = $this->getCsvLines("community_type.csv");
        $this->ctm->import($communitytypes);
        $event = $stopwatch->stop('communityType');
        echo $event ."\n";
        gc_collect_cycles();
        
        $stopwatch->start('company');
        $companies = $this->getCsvLines("company.csv");
        $this->cpm->import($companies);
        $event = $stopwatch->stop('company');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('company_membership');
        $companyMembership = $this->getCsvLines("company_membership.csv");
        $this->cpm->importMembership($companyMembership);
        $event = $stopwatch->stop('company_membership');
        echo $event ."\n";
        gc_collect_cycles();

       $stopwatch->start('script');

        $qualifier = $this->getCsvLines("qualifier.csv");
        $this->scm->importQualifier($qualifier);

        $forms = $this->getCsvLines("form.csv");
        $this->scm->importForm($forms);

        $scripts = $this->getCsvLines("script.csv");
        $this->scm->import($scripts);

       // PRETHERO SCRIPTS
        $pretheroScripts = $this->getJsonFile("scripts.json");
        $this->pretheroScriptManager->importScripts($pretheroScripts);


        $scriptsqualifier = $this->getCsvLines("script_qualifier.csv");
        $this->scm->importScriptQualifier($scriptsqualifier);
        $this->pretheroScriptManager->linkScriptsToQualifiers($pretheroScripts);

        $scriptsform = $this->getCsvLines("script_form.csv");
        $this->scm->importScriptForm($scriptsform);

        $event = $stopwatch->stop('script');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('script_lang');
        $scripts = $this->getCsvLines("script_lang.csv");
        $this->scm->importLanguage($scripts);
        $this->pretheroScriptManager->linkScriptsToLanguage($pretheroScripts);

        $event = $stopwatch->stop('script_lang');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('script_person');
        $scriptPersons = $this->getCsvLines("script_person.csv");
        $this->scm->importCreator($scriptPersons);
        $this->pretheroScriptManager->linkScriptsToPersons($pretheroScripts);

        $event = $stopwatch->stop('script_person');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('performance');
        $performances = $this->getCsvLines("performance.csv");
        $this->pfm->import($performances);

        // PRETHERO PERFORMANCES 
        $pretheroPerformances = $this->getJsonFile("performances.json");
        $this->pretheroPerformanceManager->importPerformances($pretheroPerformances);

        $event = $stopwatch->stop('performance');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('performance_person');
        $performancePersons = $this->getCsvLines("performance_person.csv");
        $this->pfm->importPerson($performancePersons);
        $event = $stopwatch->stop('performance_person');
        echo $event ."\n";
        gc_collect_cycles();

        // PREHTERO PERFORMANCES PERSON 
        $stopwatch->start('prethero_performance_person');
        $pretheroPerformancePerson = $this->getJsonFile("performance_persons.json");
        $this->pretheroPerformanceManager->linkPerformancesToPersons($pretheroPerformances, $pretheroPerformancePerson);

        $event = $stopwatch->stop('prethero_performance_person');
        echo $event ."\n";
        gc_collect_cycles();



        $stopwatch->start('performance_preferred_title');
        $performanceTitles = $this->getCsvLines("performance_preferred_title.csv");
        $this->pfm->importPreferedTitle($performanceTitles);
        $event = $stopwatch->stop('performance_preferred_title');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('performance_alternative_title');
        $performanceTitles = $this->getCsvLines("performance_alternative_title.csv");
        $this->pfm->importAlternativeTitle($performanceTitles);
        $event = $stopwatch->stop('performance_alternative_title');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publisher');
        $publishers = $this->getCsvLines("publisher.csv");
        $this->publicationManager->importPublisher($publishers);
        $event = $stopwatch->stop('publisher');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('anthologies');
        $anthology = $this->getCsvLines("anthologies.csv");
        $this->anthologyManager->import($anthology);
        $event = $stopwatch->stop('anthologies');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('anthologies_locations');
        $anthologyLocations = $this->getCsvLines("anthology_location.csv");
        $this->anthologyManager->importAnthologyPublicationLocations($anthologyLocations);
        $event = $stopwatch->stop('anthologies_locations');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publication');
        $publications = $this->getCsvLines("publication.csv");
        $this->publicationManager->importPublication($publications);
        $event = $stopwatch->stop('publication');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publication_publisher');
        $publicationsPublisher = $this->getCsvLines("publication_publisher.csv");
        $this->publicationManager->importPublicationsPublisher($publicationsPublisher);
        $event = $stopwatch->stop('publication_publisher');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publication_titles');
        $publicationTitles = $this->getCsvLines("publication_titles.csv");
        $this->publicationManager->importPublicationTitles($publicationTitles);
        $event = $stopwatch->stop('publication_titles');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publication_locations');
        $publicationLocations = $this->getCsvLines("publication_location.csv");
        $this->publicationManager->importPublicationLocations($publicationLocations);
        $event = $stopwatch->stop('publication_locations');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('publisher_person');
        $publisherMembership = $this->getCsvLines("publisher_person.csv");
        $this->publicationManager->importPublisherMembership($publisherMembership);
        $event = $stopwatch->stop('publisher_person');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('library');
        $libraries = $this->getCsvLines("library.csv");
        $this->libraryManager->importLibrary($libraries);
        $event = $stopwatch->stop('library');
        echo $event ."\n";
        gc_collect_cycles();

        // PRETHERO PUBLICATIONS - PUBLISHER 
                
        $stopwatch->start('Prethero Publications - Publishers');
        $pretheroPublications = $this->getJsonFile("carriers.json");
        $this->pretheroPublicationManager->importCarriers($pretheroPublications, $pretheroScripts);
        $this->pretheroPublicationManager->importPublishers($pretheroPublications);
        $event = $stopwatch->stop('Prethero Publications - Publishers');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('person_authorities');
        $personAuthorities = $this->getCsvLines("person_authorities.csv");
        $this->pm->importAuthorities($personAuthorities);
        $event = $stopwatch->stop('person_authorities');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('manuscript');
        $manuscripts = $this->getCsvLines("manuscript.csv");
        $this->manuscriptManager->importManuscript($manuscripts);
        $event = $stopwatch->stop('manuscript');
        echo $event ."\n";
        gc_collect_cycles();

        $stopwatch->start('copy');
        $copies = $this->getCsvLines("copy.csv");
        $this->copyManager->importCopy($copies);
        $event = $stopwatch->stop('copy');
        echo $event ."\n";
        gc_collect_cycles();

        // ALIGNEMENTS

        $BnfPublications = $this->getCsvLines("publication_bnf_align_from_copies.csv");
        $this->publicationManager->importPublicationBNFAuthorities($BnfPublications);

        $geonamesAlign = $this->getCsvLines("align_geonames_directmatches.csv");
        $this->locationManager->geonamesAlign($geonamesAlign);

        $event = $stopwatch->stop('import');
        echo $event ."\n";
    }

    public function getCsvLines($filename)
    {
        $finder = new Finder();
        $finder->files()->in("./data/");

        echo "\n ---- Read " . $filename . "\n";
        $file = $finder->files()->name($filename);
        foreach ($file as $f) {
            $csv = $f;
        }
        $lines = [];
        if (($handle = fopen($csv->getRealPath(), "r")) !== false) {
            while (($data = fgetcsv($handle, null, "\t")) !== false) {
                $lines[] = $data;
            }
            fclose($handle);
        }

        return $lines;
    }



    public function getJsonFile($filename)
    {
        $finder = new Finder();
        $finder->files()->in("./data/prethero_exports/");

        echo "\n ---- Read " . $filename . "\n";
        $file = $finder->files()->name($filename);
        foreach ($file as $f) {
            $json = $f;
        }

        $file = fopen( $json, "r" );
        $filesize = filesize( $json );
        $filetext = fread( $file, $filesize );
        fclose( $file );

        $data = json_decode($filetext);
        return $data;


    }





}
