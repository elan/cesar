<?php

namespace App\Service;

use App\Entity\Location;
use App\Entity\Person;
use App\Entity\Skill;
use App\Entity\SocialTitle;
use Doctrine\ORM\EntityManagerInterface;

class PersonManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($originalId, 
                            $birthMin, 
                            $birthMax, 
                            $deathMin, 
                            $deathMax, 
                            $gender, 
                            $firstname, 
                            $lastname, 
                            $pseudonym, 
                            $location, 
                            $socialTitle, 
                            $authorities, 
                            $sources)
    {
        $person = new Person();
        $person->setOriginalId($originalId);
        $person->setBirthMin($birthMin);
        $person->setBirthMax($birthMax);
        $person->setDeathMin($deathMin);
        $person->setDeathMax($deathMax);
        $person->setGender($gender);
        $person->setFirstname($firstname);
        $person->setLastname($lastname);
        $person->setPseudonym($pseudonym);
        $person->setNationality($location);
        $person->setSocialTitle($socialTitle);

        $person->setAuthorities($authorities);
        $person->setSources($sources);
                
        $this->em->persist($person);

        return $person;
    }

    public function importSocialTitle($lines)
    {
        echo "Import social Titles \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $title = new SocialTitle;
            $title->setOriginalId($originalId);
            $title->setName($name);
            $this->em->persist($title);
        }

        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }


    public function importAuthorities($lines)
    {
        echo "Import person Authorities \n";
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $authorities = $this->testEmpty($line[1]);
            // dirty trick to handle json in csv 
            // if empty cell, insert value = []
            $authorities = ($authorities) ? json_decode(str_replace("'", '"', $authorities), true) : [];

            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($originalId);

            if ($person) {
                $person->setauthorities($authorities);
                $this->em->persist($person);
            }

        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function import($lines)
    {
        echo "Import persons \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $birthMin = $this->testDate($line[1]);
            $birthMax = $this->testDate($line[2]);
            $deathMin = $this->testDate($line[3]);
            $deathMax = $this->testDate($line[4]);
            $gender = $this->testEmpty($line[5]);
            $firstname = $this->testEmpty($line[6]);
            // $particle = $this->testEmpty($line[7]);
            $lastname = $this->testEmpty($line[8]);
            $pseudonym = $this->testEmpty($line[9]);
            $socialTitleId = preg_replace('/[^0-9]/', '', $line[10]);
            $socialTitleId = intval($socialTitleId);
            $socialTitle = $this->em->getRepository(SocialTitle::class)->findOneByOriginalId($socialTitleId);
            $nationalityId = preg_replace('/[^0-9]/', '', $line[11]);
            $nationalityId = intval($nationalityId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($nationalityId);

            $authorities = [];
            $sources = ["origin" => "cesar"];


            $this->create($originalId, $birthMin, $birthMax, $deathMin, $deathMax, $gender, $firstname, $lastname, $pseudonym, $location, $socialTitle, $authorities, $sources);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function importSkill($lines)
    {
        echo "Import persons activities\n";
        foreach ($lines as $line) {
            $personId = preg_replace('/[^0-9]/', '', $line[0]);
            $personId = intval($personId);
            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);

            $skillId = preg_replace('/[^0-9]/', '', $line[1]);
            $skillId = intval($skillId);
            $skill = $this->em->getRepository(Skill::class)->findOneByOriginalId($skillId);

            if (!$skill || !$person) {
                echo $personId . " - " . $skillId . " blabla \n";
            }
            $person->addSkill($skill);
            $this->em->persist($person);
        }

        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "") ? new \DateTime($str) : null;
    }
}
