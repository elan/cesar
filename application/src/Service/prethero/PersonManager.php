<?php

namespace App\Service\prethero;

use App\Entity\Location;
use App\Entity\Person;
use App\Service\PersonManager as CesarPersonManager;
use App\Entity\Skill;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PretheroFindByAuthority;

class PersonManager
{

    private $cesarPersonManager;
    private $em;
    private $pretheroFindByAuthority;

    public function __construct(CesarPersonManager $cesarPersonManager, EntityManagerInterface $em, PretheroFindByAuthority $pretheroFindByAuthority)
    {
        $this->cesarPersonManager = $cesarPersonManager;
        $this->pretheroFindByAuthority = $pretheroFindByAuthority;
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    // public function pretheroKeyToUri($jsonKey)
    // {
    //     $search = "http://rdfh.ch/0119";
    //     $replace = "https://admin.dasch.swiss/resource/0119";
    //     $uri = str_replace($search, $replace, $jsonKey);
    //     return $uri;
    // }

    public function importPretheroPersons($data)
    {
        echo "Import Prethero Persons \n";

        foreach ($data as $key => $object) {

            $originalId = $key;

            $birthMin = isset($object->birthMin) ? $object->birthMin : null;
            $birthMax = isset($object->birthMax) ? $object->birthMax : null;
            $deathMin = isset($object->deathMin) ? $object->deathMin : null;
            $deathMax = isset($object->deathMax) ? $object->deathMax : null;
            $firstname = isset($object->firstname) ? $object->firstname : null;
            $lastname = isset($object->lastname) ? $object->lastname : null;
            $pseudonym = isset($object->pseudonym) ? $object->pseudonym : null;

            // DUMP DEBUG 
            if($firstname == null and $lastname == null){
                echo "PERSON without last nor first name " . $key;
            }

            // $socialTitle => voir mes notes ... 
            // $socialTitle  = isset($object->social_title_id) ? $object->social_title_id : null;
            $socialTitle = null;
            $gender  = $object->gender_id = "masculin" ? 1 : 2;

            $nationalityId = isset($object->nationality_id) ? $object->nationality_id[0] : null;
            if ($nationalityId) {
                $location = $this->em->getRepository(Location::class)->findOneByOriginalId($nationalityId);
                // $this->pretheroFindByAuthority->findLocation($this->pretheroKeyToUri($nationalityId));
            } else {
                $location = null;
            }

            $authorities = [];
            if (isset($object->authorities)) {
                $authoritiesList = $object->authorities;
                foreach ($authoritiesList as $authority) {
                    foreach ($authority as $key => $value) {
                        $authorities[$key] = $value;
                    }
                }
            }

            $authorities["prethero"] = $object->sources;
            $sources = ["origin" => ["prethero" => $object->sources]];

            $this->cesarPersonManager->create($originalId, $birthMin, $birthMax, $deathMin, $deathMax, $gender, $firstname, $lastname, $pseudonym, $location, $socialTitle, $authorities, $sources);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importPretheroPersonSkills($data)
    {
        echo "prethero persons activities\n";

        foreach ($data as $key => $object) {
            $originalId = $key;
            $skillsList = isset($object->skill_id) ? $object->skill_id : null;
            if ($skillsList) {
                $person = $this->em->getRepository(Person::class)->findOneByOriginalId($originalId);
                foreach ($skillsList as $skill) {
                    var_dump($skill);
                    switch ($skill) {
                        case "souverain ou souveraine":
                            $skill = $this->em->getRepository(Skill::class)->find(42);
                            break;
                        case "homme ou femme politique":
                            $skill = $this->em->getRepository(Skill::class)->find(41);
                            break;
                        case "musicien/chanteur":
                            $skill = $this->em->getRepository(Skill::class)->find(10);
                            break;
                        default:
                            $skill = $this->em->getRepository(Skill::class)->findOneBy(["name" => $skill]);
                            break;
                    }
                    $person->addSkill($skill);
                    $this->em->persist($person);
                }
            }
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
}
