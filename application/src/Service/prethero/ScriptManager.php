<?php

namespace App\Service\prethero;
use App\Entity\Activity;
use App\Entity\Person;
use App\Entity\Script;
use App\Entity\Qualifier;
use App\Entity\Language;

use App\Service\ScriptManager as CesarScriptManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PretheroFindByAuthority;

class ScriptManager
{

    private $cesarScriptManager;
    private $em;
    private $pretheroFindByAuthority;

    public function __construct(CesarScriptManager $cesarScriptManager, EntityManagerInterface $em, PretheroFindByAuthority $pretheroFindByAuthority)
    {
        $this->cesarScriptManager = $cesarScriptManager;
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->pretheroFindByAuthority = $pretheroFindByAuthority;
    }

    public function pretheroKeyToUri($jsonKey)
    {
        $search = "http://rdfh.ch/0119";
        $replace = "https://admin.dasch.swiss/resource/0119";
        $uri = str_replace($search, $replace, $jsonKey);
        return $uri;
    }

    public function importScripts($data)
    {
        echo "Import Prethero Scripts \n";

        foreach ($data as $key => $object) {

            $originalId = $key;

            $title = isset($object->title) ? $object->title : null;

            if($title == null){
                echo "SCRIPT WIHTOUT TITLE " . $originalId . " \n";
            }

            $subtitle = null; // SPLIT ? MORE TITLES TO COME ... 
            $alternativeTitle = null; 
            $alternativeSubtitle = null;
            $actNumber = null;
            $genre = null;

            $authorities["prethero"] = $object->sources;
            $sources = ["origin" => ["prethero" => $object->sources]];

            $this->cesarScriptManager->create($originalId, $title, $subtitle, $alternativeTitle, $alternativeSubtitle, $actNumber, $genre, $authorities, $sources);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function linkScriptsToPersons($data)
    {      
        echo "prethero prethero script -> person \n";

        foreach ($data as $key => $object) {

            if(isset($object->author_id)){
                $originalId = $key;
                foreach($object->author_id as $creator) {

                    $person = $this->em->getRepository(Person::class)->findOneByOriginalId($creator); //$person = $this->pretheroFindByAuthority->findPerson($this->pretheroKeyToUri($creator));
                    $script = $this->em->getRepository(Script::class)->findOneByOriginalId($originalId); // $script = $this->pretheroFindByAuthority->findScript($this->pretheroKeyToUri($originalId));
                    $activity = $this->em->getRepository(Activity::class)->find(5); // auteur

                    $sources = ["origin" => "prethero"];
 

                    $this->cesarScriptManager->createScriptCreator($person, $script, $activity, $sources);
                }
            }
            $this->em->flush();
            $this->em->clear();
            echo "\n";
        }
    }


    public function linkScriptsToQualifiers($data)
    {
        echo "Import prethero scripts -> qualifier \n";

        foreach ($data as $key => $object) {
            $originalId = $key;
            if(isset($object->qualifier_id)){
                foreach($object->qualifier_id as $qualifier => $qualifierName){
                    $script = $this->em->getRepository(Script::class)->findOneByOriginalId($originalId); //$script = $this->pretheroFindByAuthority->findScript($this->pretheroKeyToUri($originalId));

                    $qualifier = $this->em->getRepository(Qualifier::class)->findOneBy(["name" => $qualifierName]);
                }
                $script->addQualifier($qualifier);
                $this->em->persist($script);    
            }
        }
        $this->em->flush();
        $this->em->clear();
    }


    public function linkScriptsToLanguage($data)
    {
        echo "Import script -> language \n";

        foreach ($data as $key => $object) {
            $originalId = $key;
            if(isset($object->language_id)){
                    $script = $this->em->getRepository(Script::class)->findOneByOriginalId($originalId);                     // $script = $this->pretheroFindByAuthority->findScript($this->pretheroKeyToUri($originalId));

                    $language = $this->em->getRepository(Language::class)->findOneBy(["name" => $object->language_id]);

                    $script->addLanguage($language);
                    $this->em->persist($script);    
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

}
