<?php

namespace App\Service\prethero;
use App\Entity\Activity;
use App\Entity\Person;
use App\Entity\Language;
use App\Entity\Library;
use App\Entity\Location;
use App\Entity\Publication;
use App\Entity\Publisher;
use App\Entity\PublisherMembership;
use App\Entity\Script;

use App\Service\PublicationManager as CesarPublicationManager;
use App\Service\ManuscriptManager as CesarManuscriptManager;
use App\Service\CopyManager as CesarCopyManager;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PretheroFindByAuthority;

class PublicationManager
{

    private $cesarPublicationManager;
    private $cesarManuscriptManager;
    private $cesarCopyManager;
    private $em;
    private $pretheroFindByAuthority;

    public function __construct(CesarPublicationManager $cesarPublicationManager, 
                                CesarManuscriptManager $cesarManuscriptManager,
                                CesarCopyManager $cesarCopyManager, 
                                EntityManagerInterface $em, 
                                PretheroFindByAuthority $pretheroFindByAuthority)
    {
        $this->cesarPublicationManager = $cesarPublicationManager;
        $this->cesarManuscriptManager = $cesarManuscriptManager;
        $this->cesarCopyManager = $cesarCopyManager;
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->pretheroFindByAuthority = $pretheroFindByAuthority;
    }

    public function pretheroKeyToUri($jsonKey)
    {
        $search = "http://rdfh.ch/0119";
        $replace = "https://admin.dasch.swiss/resource/0119";
        $uri = str_replace($search, $replace, $jsonKey);
        return $uri;
    }

    public function importCarriers($carriersData, $scriptData){

        echo "Import Prethero CARRIERS  \n";

        foreach($scriptData as $scriptkey => $scriptMeta){ 

            // PRINTED PUBLICATIONS 
            if(isset($scriptMeta->publication_id)){
                foreach($scriptMeta->publication_id as $publication_id){

                    // echo "SCRIPT : ", $scriptkey, "  -> Printed in -> ",  $publication_id,   " \n" ;

                    $originalPubliId = $publication_id;
                    $object = $carriersData->$publication_id;

                    if(isset($object->start)){
                        if(is_object($object->start)) {
                            $start = isset($object->start->start) ? new \DateTime($object->start->start) : null;
                        } 
                        else {
                            $start = isset($object->start) ? new \DateTime($object->start) : null;
                        }
                    }
                    if(isset($object->end)){
                        if(is_object($object->end)) {
                            $end = isset($object->end->end) ? new \DateTime($object->end->end) : null;
                        } 
                        else {
                            $end = isset($object->end) ? new \DateTime($object->end) : null;
                        }
                    }
                        
                    $approvalStart = null;
                    $approvalEnd = null;
                    $dateComment = null; 
                    $format = null; 
                    $anthologyVolume = null;
                    $anthology = null;
        
                    $authorities = ["prethero" => $object->sources]; // Should always be there ... 
                    if(isset($object->authorities)){
                        foreach($object->authorities as $authority)
                            foreach($authority as $name => $value) {
                                $authorities[$name] = $value;
                            }
                    }
                    $sources = isset($object->sources) ? ["origin" => ["prethero" => $object->sources]] : [];
                    $script = $scriptkey;

                    $publication = $this->cesarPublicationManager->createPublication($originalPubliId, $script, $format, $start, $end, $dateComment, $approvalStart, $approvalEnd, $anthologyVolume, $anthology, $authorities, $sources);

                    // ADD PUBLICATION TITLE 
                    $title = isset($object->title) ? $object->title : null;
                    $subtitle = isset($object->subtitle) ? $object->subtitle : null;
                    $publication->setTitle($title);
                    $publication->setSubtitle($subtitle);

                    // ADD PUBLICATION LOCATION 
                    if (isset($object->location_id)){
                        // $object->location_id[0] : mieux vaut être sur qu'il n'y a jamais qu'un seul lieu associé à une publication 
                        $location = $this->em->getRepository(Location::class)->findOneByOriginalId($object->location_id[0]);
                        if(!$location){
     
                            $uuid = substr($object->location_id[0], 21);
                            $uuid = str_replace('-', '=', $uuid);
                            $location = $this->pretheroFindByAuthority->findLocation($uuid); 
                            if(!$location){
                            echo "No LOCATION FOUND FOR " . $originalPubliId . "\n"  ;
                            }
                        }
                        $publication->addLocation($location);
                    }

                    // CREATE LINKED COPY 
                    // Never happens :'( 
                    if(isset($object->call_number) or isset($object->library_id)){

                        // echo "copy" , "\n";

                        $callNumber = isset($object->call_number) ? $object->call_number : null; 
                        $library = isset($object->library_id) ?    $this->em->getRepository(Library::class)->findOneBy(["name" => $object->library_id]) : null; 
                        $description = null;
                        $sources = isset($object->sources) ? ["origin" => ["prethero" => $object->sources]] : [];
                   
                        $copy = $this->cesarCopyManager->createCopy( $originalPubliId , $publication, $library, $callNumber, $description, $sources);
                        $this->em->persist($copy);

                    }

                    // ADD GENRE TO LINKED SCRIPT 
                    // genre comes from the publication cuz it's supposed to be a transcription of the genre advertised on the printed frontispiece or anysuchthing 
                    if(isset($object->genre)){
                        $scriptEntity = $this->em->getRepository(Script::class)->findOneByOriginalId($script);
                        $scriptEntity->setGenre($object->genre);
                        $this->em->persist($scriptEntity);
                    }
                            
                }
                $this->em->persist($publication);
            }

            // MANUSCRIPTS 
            elseif(isset($scriptMeta->manuscript_id)) {
                
                foreach($scriptMeta->manuscript_id as $manuscript_id){

                    // echo "SCRIPT : ", $scriptkey, "  -> MANUSCRIPT -> ",  $manuscript_id,   " \n" ;

                    $originalManuscriptId = $manuscript_id;
                    $object = $carriersData->$manuscript_id;

                    if(isset($object->start)){
                        if(is_object($object->start)) {
                            $start = isset($object->start->start) ? new \DateTime($object->start->start) : null;
                        } 
                        else {
                            $start = isset($object->start) ? new \DateTime($object->start) : null;
                        }
                    }
                    if(isset($object->end)){
                        if(is_object($object->end)) {
                            $end = isset($object->end->end) ? new \DateTime($object->end->end) : null;
                        } 
                        else {
                            $end = isset($object->end) ? new \DateTime($object->end) : null;
                        }
                    }

                    $callNumber = isset($object->call_number) ? $object->call_number : null ;
             

                    $library = isset($object->library_id) ? $this->em->getRepository(Library::class)->findOneBy(["name" => $object->library_id]) : null;

                    // DEBUG
                    if($library == null) {
                        if (isset($object->library_id)){
                            dump($object->library_id);
                        }
                        echo "LIB NOT FOUND \n";
                    }

    
                    $description = "";

                    $authorities = ["prethero" => $object->sources]; // Should always be there ... 
                    if(isset($object->authorities)){
                        foreach($object->authorities as $authority)
                            foreach($authority as $name => $value) {
                                $authorities[$name] = $value;
                            }
                    }
                    $sources = isset($object->sources) ? ["origin" => ["prethero" => $object->sources]] : [];

                    $script =  $this->em->getRepository(Script::class)->findOneByOriginalId($scriptkey);

                    $this->cesarManuscriptManager->createManuscript($originalManuscriptId, $script, $library, $callNumber, $description, $authorities, $sources);

                    // ADD GENRE TO LINKED SCRIPT 
                    // genre comes from the publication cuz it's supposed to be a transcription of the genre advertised on the printed frontispiece or anysuchthing 
                    if(isset($object->genre)){
                        $script->setGenre($object->genre);
                        $this->em->persist($script);
                    }
                }
            }

            $this->em->flush();
            $this->em->clear();
        }
    }

    public function importPublishers($publicationData){

        echo "Import Prethero Publications -> Publishers \n";

        $newPublisherNames = [];

        foreach ($publicationData as $key => $object) {
            
            if(isset($object->publisher_id)){
                $publicationId = $key ;

                $personMembers = [];
                $publiName = "";
                $publiOriginalId = "coinedWithPersonMembers";

                foreach($object->publisher_id as $publisherPersonId){

                    $person =  $this->em->getRepository(Person::class)->findOneByOriginalId($publisherPersonId);
                    array_push($personMembers, $person); 
                    $publisherFirstName = $person->getFirstname() ? $person->getFirstname() : "";
                    $publisherLastName = $person->getLastname() ? $person->getLastname() : "";
                    $publiName = $publiName . $publisherFirstName . " " . $publisherLastName . " / ";                
                    
                }

                if($publiName == ""){
                        echo "\n EMPTY PUBLISHER NAME (jusqu'à présent, c'est Jean Durant)   \n"; 
                        dump(["PUBLLICATION ID" => $key, "PUBLISHER ID" => $object->publisher_id]);

                }
                // very DIY method to check if a publisher association was not allready created... 
                if (!array_search($publiName, $newPublisherNames)){
                    array_push( $newPublisherNames, $publiName);
    
                    $publisher = new Publisher();
                    $publisher->setOriginalId($publiOriginalId);
                    $publisher->setName(substr($publiName, 0, -2)); // to remove extra trailing "; "
                    $this->em->persist($publisher);

                    foreach($personMembers as $member){
                        $publisherMembership = new PublisherMembership();
                        $publisherMembership->setPublisher($publisher);
                        $publisherMembership->setPerson($member);
                        $publisherMembership->setActivity($this->em->getRepository(Activity::class)->find(28));
    
                        $this->em->persist($publisherMembership);
                    }
                }
                else // if publisher is allready set ... 
                {
                    $publisher =  $this->em->getRepository(Publisher::class)->findOneBy(["name" => $publiName]);
                }
                $publication =  $this->em->getRepository(Publication::class)->findOneByOriginalId($publicationId);
                $publication->setPublisher($publisher);

                $this->em->persist($publication);
            }

            $this->em->flush();
            $this->em->clear();

        }
    }
}



