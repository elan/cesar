<?php

namespace App\Service\prethero;
use App\Entity\Activity;
use App\Entity\Person;
use App\Entity\Script;
use App\Entity\Location;
use App\Entity\Performance;

use App\Service\PerformanceManager as CesarPerformanceManager;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PretheroFindByAuthority;

class PerformanceManager
{

    private $cesarPerformanceManager;
    private $em;
    private $pretheroFindByAuthority;

    public function __construct(CesarPerformanceManager $cesarPerformanceManager, EntityManagerInterface $em, PretheroFindByAuthority $pretheroFindByAuthority)
    {
        $this->cesarPerformanceManager = $cesarPerformanceManager;
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->pretheroFindByAuthority = $pretheroFindByAuthority;
    }

    public function pretheroKeyToUri($jsonKey)
    {
        $search = "http://rdfh.ch/0119";
        $replace = "https://admin.dasch.swiss/resource/0119";
        $uri = str_replace($search, $replace, $jsonKey);
        return $uri;
    }

    public function importPerformances($data)
    {
        echo "Import Prethero Performances \n";
        echo count((array) $data) . "\n";


        foreach ($data as $key => $object) {

            $originalId = $key;
            if(isset($object->dates)){
                $start =  new \DateTime($object->dates[0]->start);
                $end =  new \DateTime($object->dates[0]->end);
            }

            $scriptId = isset($object->script_id) ? $object->script_id : null;
            if ($scriptId == null){
                echo "NO SCRIPT for PERF in JSON " . $originalId  . "\n";
                $script = null;
            }
            else {
                $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

                if(!isset($script)){
                    $scriptId =  is_array($scriptId) ? json_encode($scriptId): $scriptId ;
                    echo  "\n" . "SCRIPT  " . $scriptId  .   "  NOT FOUND for PERF  " . $originalId  . "\n" . "\n";
                }
            }
                if(isset($object->location_id)){
                    if(count($object->location_id) == 1){
                        $location = $this->em->getRepository(Location::class)->findOneByOriginalId($object->location_id);
                    }
                    else {
                        $locList = []; 
                        foreach($object->location_id as $loc){

                            $uuid = substr($loc, 21);
                            $uuid = str_replace('-', '=', $uuid);

                            // echo "$loc" . "  ->  " . "$uuid" ."\n" ;
                            $location = $this->pretheroFindByAuthority->findLocation($uuid); 

                            $types = [];
                            foreach ($location->getTypes() as $type){
                                $types[] = $type->getName();
                            }
                            $locList[$location->getId()] = [
                                "name" => $location->getName(),
                                "types" => $types,
                                "parent" => $location->getParent()->getId(),

                            ]; 
                         }
                        if(count($locList) == 2){
                            foreach($locList as $loc){
                                if(array_key_exists($loc["parent"], $locList)){
                                    $location = $this->em->getRepository(Location::class)->findOneById($loc);
                                    echo "\n" . "choose " . $loc["name"] . " as precisest location"  . "\n" ; 
                                }
                                else{ $location = null;}
                            }
                        }
                        else {
                            foreach($locList as $loc){
                                foreach($loc["types"] as $type){
                                    if ($type == "localité"){
                                        $location = $this->em->getRepository(Location::class)->findOneById($loc);
                                        echo "\n" . "choose " . $loc["name"] . " as precisest location"  . "\n" ; 
                                        break 2;
                                    }
                                else{ $location = null;}
                                }
                            }
                         //   dump($locList);
                        }

                        // dump($locList);

                        if ($location = null){
                            echo "PERFORMANCE Missing LOCATION \n";
                            dump($key, $locList);
                        }

                     //   $location = null;
                    
                }
                $actNumber = null;
                $company = null; // :( ... 

                $authorities["prethero"] = $object->sources;
                $sources = ["origin" => ["prethero" => $object->sources]];
        
                $performance = $this->cesarPerformanceManager->create($originalId, $start, $end, $actNumber, $company, $script, $location, $authorities, $sources);
            }

        }
        $this->em->flush();
        $this->em->clear();
        

        // TITLES : MORE TO DO LATER ? 
        foreach ($data as $key => $object) {
            $title = isset($object->title) ? $object->title : null;
            $performance =  $this->em->getRepository(Performance::class)->findOneByOriginalId($key);
            $performance->setTitle($title);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }


    public function linkPerformancesToPersons($dataPerformances, $dataPerfPersons)
    {      
        echo "prethero performances -> performance_person \n";

        foreach ($dataPerformances as $key => $object) {

            $performanceId = $key;

            if(isset($object->performance_person_id)){
        
                foreach($object->performance_person_id as $participation) {

                    if (isset($dataPerfPersons->$participation->person_id)){
                        $personId = $dataPerfPersons->$participation->person_id; 
                        $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);
                        $performance = $this->em->getRepository(Performance::class)->findOneByOriginalId($performanceId);
    
                        $authorities["prethero"] = $dataPerfPersons->$participation->sources;
                        $sources = ["origin" => ["prethero" => $dataPerfPersons->$participation->sources]];
        
                        foreach($dataPerfPersons->$participation->activity_id as $activityId) {
                            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityId);
                            $this->cesarPerformanceManager->createPerformancePerson($person, $performance, $activity, $authorities, $sources);
                        }
                    }
                    else {
                        echo "MISSING KEY in PERF-PERSON  " . $participation . "\n";
                    }
                   
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";

    }




}
