<?php

namespace App\Service\prethero;

use App\Entity\LocationType;
use App\Entity\Location;
use App\Service\LocationManager as CesarLocationManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PretheroFindByAuthority;


class LocationManager
{

    private $lm;
    private $em;
    private $pretheroFindByAuthority;

    public function __construct(CesarLocationManager $lm, EntityManagerInterface $em, PretheroFindByAuthority $pretheroFindByAuthority)
    {
        $this->lm = $lm;
        $this->pretheroFindByAuthority = $pretheroFindByAuthority;
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }



    // public function pretheroKeyToUri($jsonKey)
    // {
    //     $search = "http://rdfh.ch/0119";
    //     $replace = "https://admin.dasch.swiss/resource/0119";
    //     $uri = str_replace($search, $replace, $jsonKey);
    //     return $uri;
    // }

    public function pretheroLocations($data)
    {
        echo "Import JSON location \n";

        $alreadyExistingLocations = [];

        foreach ($data as $key => $object) {

            $originalId = $key;

            if (isset($object->name)) {
                $name = $object->name;
                $location = $this->em->getRepository(Location::class)->findOneBy(['name' => $name]);
            } else {
                echo "LOCATION WITHOUT NAME  " . $key . "  still added under the name 'Sans Nom'" . "\n" ; 
                $name = "Sans nom"; // ne devrait plus exister avec des données propres ... pourrait simplifier l'expression du dessus
            }

            $authorities = isset($object->authorities) ? get_object_vars($object->authorities) : [];
            $authorities["prethero"] = $object->sources; // $this->pretheroKeyToUri($originalId);
            $sources = isset($object->sources) ? ["origin" => ["prethero" => $object->sources] /* $this->pretheroKeyToUri($object->sources)]*/] : [];

            $address = null;
            $startMin = null;
            $startMax = null;
            $endMin = null;
            $endMax = null;

            if ($location) {
              
                // si un lieu de même nom a été trouvé dans la base ... 
                // store pour traiter lors du parent ... 
                $alreadyExistingLocations[$originalId] = $location->getId();

                echo "LOCATION ALLREADY EXISTS " .  $location->getName() . " " . $location->getId() . " Added in 'authorities'" . "\n";

                // et on append à authorities au lieu de créer le lieu ... 
                $locAuthorites = $location->getAuthorities();
                $locAuthorites["prethero"] = $object->sources; /*$this->pretheroKeyToUri($originalId)*/;
                $location->setAuthorities($locAuthorites);
            } else {
                // echo ("Create  " . $name . "\n");
                $this->lm->create($originalId, $name, $address, $startMin, $startMax, $endMin, $endMax, $authorities, $sources);
            }
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";

        echo "Assign Prethero Locations Parents \n \n";

        foreach ($data as $key => $object) {

            $originalId = $key;

            $name = (isset($object->name)) ? $object->name : "sans nom";
           //  echo ("Finding parent of " . $name . "\n");

            // SI UN LIEU EXISTE DEJA, ON NE LUI AJOUTE PAS UN PARENT ... 
            if (!isset($alreadyExistingLocations[$originalId])) {
                if (isset($object->parent_id)) {

                    $parentId = $object->parent_id;

                    if (isset($alreadyExistingLocations[$parentId])) {

                      //  echo " parent already set \n ";
                        $originalParentId = $alreadyExistingLocations[$parentId];
                        // acces au parent par ID symfony des lieux qui existaient déjà. 
                        $parent = $this->em->getRepository(Location::class)->find($originalParentId);
                        $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
                    } else {
                      //  echo (" Adding new parent   ");

                      //  echo "original_id: " .  $originalId . "   parent_id: " . $parentId  . "\n" ;
       
                        $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
                        $parent = $this->em->getRepository(Location::class)->findOneByOriginalId($parentId);
                    }
                    $location->setParent($parent);
                    $this->em->persist($location);
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
    }


    public function pretheroLocationsLocationType($data)
    {
        echo "Assign prethero location - type \n";

        foreach ($data as $key => $object) {
            $originalId = $key;

            $authorityId = $object->sources;

            if (isset($object->location_type_id)) {
                $type = $this->em->getRepository(LocationType::class)->findOneBy(["name" => $object->location_type_id]);
                if (isset($type)) {
                    // If location was a Prethero original -> we find it with finOneByOriginalId 

                    $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
                    if (isset($location)) {
                        $location->addType($type);
                        $this->em->persist($location);
                    } else {

                       //  $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
                         $location = $this->pretheroFindByAuthority->findLocation($authorityId); 
                         if (isset($location)) {
                            $location->addType($type);
                            $this->em->persist($location);
                         }
                         else  {
                        // // foutu nom manquant 
                        $name = isset($object->name) ? $object->name : "_pas_de_nom_";
                        echo "LOCATION - TYPE WITH LOCATION NOT FOUND : " . $name . "  " . $key . "\n";

                         }

                    }
                }
            }
            $this->em->flush();
            $this->em->clear();

        }
        echo "\n";

    }
}
