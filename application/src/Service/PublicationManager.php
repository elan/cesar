<?php

namespace App\Service;

use App\Entity\Anthology;
use App\Entity\Location;
use App\Entity\Publication;
use App\Entity\Publisher;
use App\Entity\PublisherMembership;
use App\Entity\Script;
use App\Entity\Person;
use App\Entity\Activity;
use Doctrine\ORM\EntityManagerInterface;

class PublicationManager
{
    private $em;
    private $logManager;

    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function createPublisher($originalId, $name, $authorities, $sources)
    {
        $publisher = new Publisher();
        $publisher->setOriginalId($originalId);
        $publisher->setName($name);
        $publisher->setAuthorities($authorities);
        $publisher->setSources($sources);

        $this->em->persist($publisher);

        return $publisher;
    }

    public function createPublication($originalId, $scriptId, $format, $start, $end, $dateComment, $approvalStart, $approvalEnd, $anthologyVolume, $anthology, $authorities, $sources)
    {
        $publication = new Publication();

        $publication->setOriginalId($originalId);
        $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

        $publication->setScript($script);
        $publication->setFormat($format);
        $publication->setStart($start);
        $publication->setEnd($end);
        $publication->setDateComment($dateComment);
        $publication->setApprovalStart($approvalStart);
        $publication->setApprovalEnd($approvalEnd);
        $publication->setAnthologyVolume($anthologyVolume);
        $publication->setAnthology($anthology);
        $publication->setAuthorities($authorities);
        $publication->setSources($sources);


        $this->em->persist($publication);

        return $publication;
    }

    public function importPublicationsPublisher($lines)
    {
        echo "Import publication publisher \n";

        foreach ($lines as $line) {
            $publicationId = preg_replace('/[^0-9]/', '', $line[2]);
            $publicationId = intval($publicationId);
            $publisherId = preg_replace('/[^0-9]/', '', $line[1]);
            $publisherId = intval($publisherId);
            $publication = $this->em->getRepository(Publication::class)->findOneByOriginalId($publicationId);
            $publisher = $this->em->getRepository(Publisher::class)->findOneByOriginalId($publisherId);
            $publication->setPublisher($publisher);

            $this->em->persist($publication);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importPublisherMembership($lines)
    {
        echo "Import publisher membership \n";

        $this->logManager->write("\n\nImport publisher membership \n");
        $this->logManager->write("publisherId | personId | activityId \n");

        foreach ($lines as $line) {
            $publisherId = preg_replace('/[^0-9]/', '', $line[0]);
            $publisherId = intval($publisherId);

            $personId = preg_replace('/[^0-9]/', '', $line[1]);
            $personId = intval($personId);

            $activityId = preg_replace('/[^0-9]/', '', $line[2]);
            $activityId = intval($activityId);

            // $startMin = $this->testDate($line[3]);
            // $startMax = $this->testDate($line[4]);
            // $endMin = $this->testDate($line[5]);
            // $endMax = $this->testDate($line[6]);

            $sources = ["origin" => "cesar"];

            
            $publisher = $this->em->getRepository(Publisher::class)->findOneByOriginalId($publisherId);
            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);
            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityId);

            if (!$publisher || !$person || !$activity) {
                $this->logManager->write($publisherId . " " . $personId . " " .$activityId . "\n");

            } else {
                $publisherMembership = new PublisherMembership();
                $publisherMembership->setPublisher($publisher);
                $publisherMembership->setPerson($person);
                $publisherMembership->setActivity($activity);
                $publisherMembership->setSources($sources);
            
                $this->em->persist($publisherMembership);
            }
            
          
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
    

    public function importPublicationTitles($lines)
    {
        echo "Import publication titles \n";

        foreach ($lines as $line) {
            $publicationId = preg_replace('/[^0-9]/', '', $line[0]);
            $publicationId = intval($publicationId);

            $title = $this->testEmpty($line[3]);
            $subtitle = $this->testEmpty($line[4]);
            $alternativeTitle = $this->testEmpty($line[5]);
            $alternativeSubtitle = $this->testEmpty($line[6]);

            $publication = $this->em->getRepository(Publication::class)->findOneByOriginalId($publicationId);

            $publication->setTitle($title);
            $publication->setSubtitle($subtitle);
            $publication->setAlternativeTitle($alternativeTitle);
            $publication->setAlternativeSubtitle($alternativeSubtitle);
            $this->em->persist($publication);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }


    public function importPublication($lines)
    {
        echo "Import publication \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $scriptId = preg_replace('/[^0-9]/', '', $line[1]);
            $scriptId = intval($scriptId);

            $format = $this->testEmpty($line[2]);

            $start = $this->testDate($line[3]);
            $end = $this->testDate($line[4]);
            $dateComment = $this->testEmpty($line[8]);

            $approvalStart = $this->testDate($line[9]);
            $approvalEnd = $this->testDate($line[10]);

            $authorities = [];
            $sources = ["origin" => "cesar"];

            $anthologyVolume = $this->testEmpty($line[11]);
            $anthologyVolume = intval($anthologyVolume);

            $anthologyId = preg_replace('/[^0-9]/', '', $line[7]);
            $anthologyId = intval($anthologyId);
            $anthology = $this->em->getRepository(Anthology::class)->findOneByOriginalId($anthologyId);

            $this->createPublication($originalId, $scriptId, $format, $start, $end, $dateComment, $approvalStart, $approvalEnd, $anthologyVolume, $anthology, $authorities, $sources);
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importPublisher($lines)
    {
        echo "Import publisher \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);

            $authorities = [];
            $sources = ["origin" => "cesar"];

            $this->createPublisher($originalId, $name, $authorities, $sources);
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }


    public function importPublicationLocations($lines)
    {
        echo "Import publication location \n";

        $this->logManager->write("\n\nImport publication location \n");
        $this->logManager->write("publicationId | locationId \n");

        foreach ($lines as $line) {
            $publicationId = preg_replace('/[^0-9]/', '', $line[0]);
            $publicationId = intval($publicationId);

            $locationId = preg_replace('/[^0-9]/', '', $line[1]);
            $locationId = intval($locationId);
            
            $publication = $this->em->getRepository(Publication::class)->findOneByOriginalId($publicationId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($locationId);

            if (!$publication || !$location ) {
                $this->logManager->write($publicationId . " " . $locationId . "\n");

            } else {
                $publication->addLocation($location);
                $this->em->persist($publication);
            }
            
          
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importPublicationBNFAuthorities($lines)
    {
        echo "Associate Publications with BNF Authorities \n";

        array_shift($lines); // removes header. 
        foreach ($lines as $line) {
            
            $publicationId = preg_replace('/[^0-9]/', '', $line[0]);
            $publicationId = intval($publicationId);
            $publication = $this->em->getRepository(Publication::class)->findOneById($publicationId);

            $authorities = $this->testEmpty($line[2]);
            // dirty trick to handle json in csv 
            $authorities = ($authorities) ? json_decode(str_replace("'", '"', $authorities), true) : [];

            $publication->setAuthorities($authorities);
            $this->em->persist($publication);
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
