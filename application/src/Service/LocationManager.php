<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\LocationType;
use App\Entity\Location;

class LocationManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function createType($originalId, $name, $description, $authorities)
    {
        $locationType = new LocationType();
        $locationType->setOriginalId($originalId);
        $locationType->setName($name);
        $locationType->setDescription($description);
        $locationType->setAuthorities($authorities);


        $this->em->persist($locationType);

        return $locationType;
    }

    public function create($originalId, $name, $address, $startMin, $startMax, $endMin, $endMax, $authorities, $sources)
    {
        $location = new Location();
        $location->setOriginalId($originalId);
        $location->setName($name);
        $location->setAddress($address);
        $location->setStartMin($startMin);
        $location->setStartMax($startMax);
        $location->setEndMin($endMin);
        $location->setEndMax($endMax);

        $location->setAuthorities($authorities);
        $location->setSources($sources);

        $this->em->persist($location);

        return $location;
    }


    public function import($lines)
    {
        echo "Import location \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $address = $this->testEmpty($line[2]);

            $startMin = $this->testDate($line[3]);
            $startMax = $this->testDate($line[4]);
            $endMin = $this->testDate($line[5]);
            $endMax = $this->testDate($line[6]);

            $authorities = [];
            $sources = ["origin" => "cesar"];

            $this->create($originalId, $name, $address, $startMin, $startMax, $endMin, $endMax, $authorities, $sources);
        }
        $this->em->flush();
        $this->em->clear();
        
        // parent
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $parentId = preg_replace('/[^0-9]/', '', $line[7]);
            $parentId = intval($parentId);

            $parent = $this->em->getRepository(Location::class)->findOneByOriginalId($parentId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($originalId);
            if ($parent) {
                $location->setParent($parent);
                $this->em->persist($location);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importLocationsLocationType($lines){
        echo "Assign location type \n";
        foreach ($lines as $line) {
            $locationId = preg_replace('/[^0-9]/', '', $line[0]);
            $locationId = intval($locationId);
            $location = $this->em->getRepository(Location::class)->findOneByOriginalId($locationId);

            $typeId = preg_replace('/[^0-9]/', '', $line[1]);
            $typeId = intval($typeId);
            $type = $this->em->getRepository(LocationType::class)->findOneByOriginalId($typeId);

            $location->addType($type);
            $this->em->persist($location);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }


    public function importType($lines)
    {
        echo "Import location type \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $name = $this->testEmpty($line[1]);
            $description = $this->testEmpty($line[3]);

            $authorities = [];


            $this->createType($originalId, $name, $description, $authorities);
        }
        $this->em->flush();
        $this->em->clear();

        // parent
        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $parentId = preg_replace('/[^0-9]/', '', $line[2]);
            $parentId = intval($parentId);

            $parent = $this->em->getRepository(LocationType::class)->findOneByOriginalId($parentId);
            $locationType = $this->em->getRepository(LocationType::class)->findOneByOriginalId($originalId);
            if ($parent) {
                $locationType->setParent($parent);
                $this->em->persist($locationType);
            }
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function geonamesAlign($lines)
    {
        echo "Associate Locations with Geonames + lat and long \n";

        array_shift($lines); 
        foreach ($lines as $line) {
            $locationId = preg_replace('/[^0-9]/', '', $line[0]);
            $locationId = intval($locationId);
            $location = $this->em->getRepository(Location::class)->findOneById($locationId);

            $geonamesUri = $this->testEmpty($line[6]);
            // dirty trick to handle json in csv 
            //$geonamesUri = ($geonamesUri) ? json_decode(str_replace("'", '"', $geonamesUri), true) : [];


            $authorities =  $location->getAuthorities();
            if (null != $authorities){
                if (!isset($authorities["geonames"])){
                    $authorities["geonames"] = $geonamesUri;
                    $location->setAuthorities($authorities);
                }
                elseif($authorities["geonames"] == null){
                    $authorities["geonames"] = $geonamesUri;
                    $location->setAuthorities($authorities);
                }
            }
            else {
                $new_authority["geonames"] = $geonamesUri;
                $location->setAuthorities($new_authority);
            }
            
            $lat = $this->testEmpty($line[11]);
            $lng = $this->testEmpty($line[12]);
            $location->setLat($lat);
            $location->setLng($lng);

        }
        $this->em->flush();
        $this->em->clear();

    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
