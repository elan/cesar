<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Note;
use App\Entity\Person;
use App\Entity\User;

class NoteManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($target, $content, $originalId)
    {
        $note = new Note();
        $note->setTarget($target);
        $note->setContent($content);
        $note->setOriginalId($originalId);
        $this->em->persist($note);

        return $note;
    }

    public function import($lines)
    {
        echo "Import note \n";

        foreach ($lines as $line) {
            $targetId = preg_replace('/[^0-9]/', '', $line[0]);
            $targetId = intval($targetId);
            $target = $line[1] ? trim($line[1]) : null;
            $content = trim($line[2]);
            $originalId = preg_replace('/[^0-9]/', '', $line[3]);
            $originalId = intval($originalId);
            $targetTable = trim($line[4]);

            if ($targetTable == "tsar_person") {
                $person = $this->em->getRepository(Person::class)->findOneByOriginalId($targetId);
                if ($person) {
                    $note = $this->create($target, $content, $originalId);
                    $person->addNote($note);
                    $this->em->persist($person);
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }

    public function importAuthor($lines)
    {
        echo "Import note authors \n";
        foreach ($lines as $line) {
            $noteId = preg_replace('/[^0-9]/', '', $line[0]);
            $noteId = intval($noteId);
            $note = $this->em->getRepository(Note::class)->findOneByOriginalId($noteId);
            if ($note) {
                $userId = preg_replace('/[^0-9]/', '', $line[1]);
                $userId = intval($userId);
                $user = $this->em->getRepository(User::class)->findOneByOriginalId($userId);

                $user->addNote($note);
                $this->em->persist($user);
            }
        }

        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
}
