<?php

namespace App\Service;

use App\Entity\Activity;
use App\Entity\Form;
use App\Entity\Person;
use App\Entity\Language;
use App\Entity\Qualifier;
use App\Entity\Script;
use App\Entity\ScriptCreator;
use Doctrine\ORM\EntityManagerInterface;

class ScriptManager
{
    private $em;
    private $logManager;
    public function __construct(EntityManagerInterface $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
        $this->logManager = $logManager;
    }

    public function create($originalId, $title, $subtitle, $alternativeTitle, $alternativeSubtitle, $actNumber, $genre, $authorities, $sources)
    {
        $script = new Script;
        $script->setOriginalId($originalId);
        $script->setTitle($title);
        $script->setSubtitle($subtitle);
        $script->setAlternativeTitle($alternativeTitle);
        $script->setAlternativeSubtitle($alternativeSubtitle);
        $script->setActNumber($actNumber);
        $script->setGenre($genre);
        $script->setAuthorities($authorities);
        $script->setSources($sources);



        $this->em->persist($script);

        return;
    }

    public function import($lines)
    {
        echo "Import scripts \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $actNumber = preg_replace('/[^0-9]/', '', $line[1]);
            $actNumber = intval($actNumber) != 0 ? intval($actNumber) : null;

            $title = $line[2];
            $subtitle = $this->testEmpty($line[3]);
            $alternativeTitle = $this->testEmpty($line[4]);
            $alternativeSubtitle = $this->testEmpty($line[5]);
            $genre = $this->testEmpty($line[6]);

            $sources = ["origin" => "cesar"];
            $authorities = [];


            $this->create($originalId, $title, $subtitle, $alternativeTitle, $alternativeSubtitle, $actNumber, $genre, $authorities, $sources);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }

    public function createScriptCreator($person, $script, $activity, $sources)
    {
        $scriptCreator = new ScriptCreator();
        $scriptCreator->setPerson($person);
        $scriptCreator->setActivity($activity);
        $scriptCreator->setScript($script);
        $scriptCreator->setSources($sources);


        $this->em->persist($scriptCreator);

        return;
    }


    public function importForm($lines)
    {
        echo "Import forms \n";

        foreach ($lines as $line) {
            $formId = preg_replace('/[^0-9]/', '', $line[0]);
            $formId = intval($formId);

            $name = $this->testEmpty($line[1]);

            $authorities = [];

            $form = new Form;
            $form->setOriginalId($formId);
            $form->setName($name);
            $form->setAuthorities($authorities);

            $this->em->persist($form);
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importQualifier($lines)
    {
        echo "Import qualifiers \n";

        foreach ($lines as $line) {
            $qualifierId = preg_replace('/[^0-9]/', '', $line[0]);
            $qualifierId = intval($qualifierId);

            $name = $this->testEmpty($line[1]);
            $description = $this->testEmpty($line[2]);

            $authorities = [];

            $qualifier = new Qualifier;
            $qualifier->setOriginalId($qualifierId);
            $qualifier->setName($name);
            $qualifier->setDescription($description);
            $qualifier->setAuthorities($authorities);

            $this->em->persist($qualifier);
        }
        $this->em->flush();
        $this->em->clear();

        foreach ($lines as $line) {
            $qualifierId = preg_replace('/[^0-9]/', '', $line[0]);
            $qualifierId = intval($qualifierId);
            $qualifier = $this->em->getRepository(Qualifier::class)->findOneByOriginalId($qualifierId);

            $parentId = preg_replace('/[^0-9]/', '', $line[3]);
            $parentId = intval($parentId);
            $parent = $this->em->getRepository(Qualifier::class)->findOneByOriginalId($parentId);
            $qualifier->setParent($parent);

            $this->em->persist($qualifier);
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importScriptQualifier($lines)
    {
        echo "Import script qualifier \n";

        foreach ($lines as $line) {
            $scriptId = preg_replace('/[^0-9]/', '', $line[0]);
            $scriptId = intval($scriptId);
            $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

            $qualifierId = preg_replace('/[^0-9]/', '', $line[1]);
            $qualifierId = intval($qualifierId);
            $qualifier = $this->em->getRepository(Qualifier::class)->findOneByOriginalId($qualifierId);

            if ($qualifier) {
                $script->addQualifier($qualifier);
                $this->em->persist($script);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importScriptForm($lines)
    {
        echo "Import script forms \n";

        foreach ($lines as $line) {
            $scriptId = preg_replace('/[^0-9]/', '', $line[0]);
            $scriptId = intval($scriptId);
            $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

            $formId = preg_replace('/[^0-9]/', '', $line[1]);
            $formId = intval($formId);
            $form = $this->em->getRepository(Form::class)->findOneByOriginalId($formId);

            if ($form) {
                $script->addForm($form);
                $this->em->persist($script);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }
    

    public function importLanguage($lines)
    {
        echo "Import script languages \n";

        foreach ($lines as $line) {
            $scriptId = preg_replace('/[^0-9]/', '', $line[0]);
            $scriptId = intval($scriptId);
            $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

            $languageCode = $this->testEmpty($line[1]);
            $language = $this->em->getRepository(Language::class)->findOneByCode($languageCode);

            if ($language) {
                $script->addLanguage($language);
                $this->em->persist($script);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importCreator($lines)
    {
        echo "Import script creators \n";

        $this->logManager->write("\n\nImport script creators \n");
        $this->logManager->write("person | script | activity \n");

        foreach ($lines as $line) {
            $activityId = preg_replace('/[^0-9]/', '', $line[0]);
            $activityId = intval($activityId);
            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityId);

            $personId = preg_replace('/[^0-9]/', '', $line[1]);
            $personId = intval($personId);
            $person = $this->em->getRepository(Person::class)->findOneByOriginalId($personId);

            $scriptId = preg_replace('/[^0-9]/', '', $line[2]);
            $scriptId = intval($scriptId);
            $script = $this->em->getRepository(Script::class)->findOneByOriginalId($scriptId);

            $sources = ["origin" => "cesar"];

            if (!$script || !$person || !$activity) {
                $this->logManager->write($personId . " " . $scriptId . " " . $activityId . "\n");
            } else {
                $this->createScriptCreator($person, $script, $activity, $sources);
            }
        }

        $this->em->flush();
        $this->em->clear();
    }

    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "" && $str != "NULL") ? new \DateTime($str) : null;
    }
}
