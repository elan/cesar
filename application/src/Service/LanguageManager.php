<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Language;

class LanguageManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($code, $name, $authorities)
    {
        $language = new Language();
        $language->setCode($code);
        $language->setName($name);
        $language->setAuthorities($authorities);

      
        $this->em->persist($language);

        return $language;
    }

    public function import($lines)
    {
        echo "Import languages \n";

        foreach ($lines as $line) {
            $code = $this->testEmpty($line[1]);
            $name = $this->testEmpty($line[2]);

            $authorities = [];

            $this->create($code, $name, $authorities);
        }
        $this->em->flush();
        $this->em->clear();

        echo "\n";
    }
    
    public function testEmpty($str)
    {
        return ($str != "" && $str != "NULL") ? $str : null;
    }

    public function testDate($str)
    {
        return ($str != "") ? new \DateTime($str) : null;
    }
}
