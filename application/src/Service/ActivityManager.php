<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Activity;

class ActivityManager
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setMiddlewares([]);
    }

    public function create($originalId, $name, $description, $authorities)
    {
        $activity = new Activity();

        $activity->setOriginalId($originalId);
        $activity->setDescription($description);
        $activity->setName($name);
        $activity->setAuthorities($authorities);


        $this->em->persist($activity);

        return $activity;
    }

    public function importParent($lines)
    {
        echo "Import parent activity \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);

            $activity = $this->em->getRepository(Activity::class)->findOneByOriginalId($originalId);
            $activityOriginalId = ($line[6] != "") ? trim($line[6]) : null;
            if($activityOriginalId){
                $parent = $this->em->getRepository(Activity::class)->findOneByOriginalId($activityOriginalId);
                if ($parent) {
                    $activity->setParent($parent);
                    $this->em->persist($activity);
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function import($lines)
    {
        echo "Import activity \n";

        foreach ($lines as $line) {
            $originalId = preg_replace('/[^0-9]/', '', $line[0]);
            $originalId = intval($originalId);
            $name = trim($line[1]);
            $description = ($line[5] != "") ? trim($line[5]) : null;

            $authorities = [];

            $this->create($originalId, $name, $description, $authorities);
        }
        $this->em->flush();
        $this->em->clear();
        echo "\n";
    }
}
