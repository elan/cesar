const Mirador = require('mirador/dist/mirador.min.js');

var detectManifest = document.getElementsByClassName("manifest-uri");
console.log(detectManifest);


for(var i = 0; i < detectManifest.length; i++) {
  // uris.push(detectManifest[i].dataset.uri);

  var manifest = detectManifest[i].dataset.uri;
  console.log("manifest", detectManifest[i].dataset.uri);

  var targetEl = detectManifest[i].target;
  console.log("targetElement", detectManifest[i].target);


  var mirador = Mirador.viewer({
    "id": targetEl ,
    "manifests": {
      manifest: {
      }
    },
    "language": 'fr',
    "windows": [
      {
        // "allowWindowSideBar": true // menu trois tirets
        "allowClose": false,
        "sideBarOpenByDefault": true, /* chemin de fer */
        "loadedManifest": manifest,
        "canvasIndex": 1, /* beginning page */
        "thumbnailNavigationPosition": 'far-right',
        "allowFullscreen": true, // Configure to show a "fullscreen" button in the WindowTopBar
        "allowMaximize": false,
  
      }
    ],
    "workspaceControlPanel": {
      "enabled": false, // Configure if the control panel should be rendered.  
                        // Useful if you want to lock the viewer down to only the configured manifests
    },
  });


}



// if (id.substring(0,21) == "http://gallica.bnf.fr"){
//     var manifest = "https://gallica.bnf.fr/iiif/" + id.substring(22) + "/manifest.json";

// }
// else {
//     var manifest = null; 
// }

// console.log(id.substring(0,21));

// console.log("id", id);
