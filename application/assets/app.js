import './styles/app.css';

import * as bootstrap from 'bootstrap'
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';

import DataTable from 'datatables.net-bs5';
import languageFR from 'datatables.net-plugins/i18n/fr-FR.json';

const Mirador = require('mirador/dist/mirador.min.js');
// mirador.min.js is required here, but used in /assets/js/mirador-call.js (which accesses to the const var Mirador). mirador-call.js is loaded from only 1 twig template : publication/mirador.viewer.html.twig

document.addEventListener('DOMContentLoaded', function () {
    let table = new DataTable('#datatable', {
        paging: true,
        ordering: true,
        lengthChange: false,
        pageLength: 30,
        language: languageFR
    });
});
